(function ($, Drupal, drupalSettings) {

  "use strict";

  /**
   * Attaches the JS countdown behavior
   */
  Drupal.behaviors.jsCountdownTimer = {

    attach: function (context) {

      console.log(drupalSettings.event_start_timestamp);


// Set the date we're counting down to
var countDownDate = new Date(drupalSettings.event_start_timestamp * 1000).getTime();

// Set the end date
var endDate = new Date(drupalSettings.event_end_timestamp * 1000).getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  var formattedDateString = '';

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var formattedDays = ("0" + days).slice(-2);
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var formattedHours = ("0" + hours).slice(-2);
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var formattedMinutes = ("0" + minutes).slice(-2);
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  var formattedSeconds = ("0" + seconds).slice(-2);

  let dateStrings = new Array();
  dateStrings['@days'] = days + ' ' + Drupal.t(' day');
  if (days > 1) {
    dateStrings['@days'] = days +  ' ' +  Drupal.t(' days');
  }
  dateStrings['@hours'] = hours +  ' ' +  Drupal.t('hour');
  if (hours == 0) {
    dateStrings['@hours'] = hours +  ' ' +  Drupal.t('hours');
  }  
  if (hours > 1) {
    dateStrings['@hours'] = hours +  ' ' +  Drupal.t('hours');
  }
  dateStrings['@minutes'] = minutes +  ' ' +  Drupal.t('minute');
  if (minutes > 1) {
    dateStrings['@minutes'] = minutes +  ' ' + Drupal.t('minutes');
  }
  dateStrings['@seconds'] = seconds +  ' ' + Drupal.t('second');
  if (seconds > 1) {
    dateStrings['@seconds'] = seconds +  ' ' + Drupal.t('seconds');
  }  

  if (days == 0) {
    if (hours == 0) {
      if (minutes == 0) {
        var formattedDate = Drupal.t('@seconds.', dateStrings);
      }
      else {
        var formattedDate = Drupal.t('@minutes.', dateStrings);
      }      
    }
    else {
      var formattedDate = Drupal.t('@hours, @minutes.', dateStrings);
    }
  } else {
    var formattedDate = Drupal.t('@days, @hours, @minutes.', dateStrings);
  }


  // Display the result in the element with id="demo"
  document.getElementById("timer__display").innerHTML = formattedDate;

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    if (now > endDate) {
      document.getElementById("timer__display").innerHTML = Drupal.t('Event has passed');
    } 
    else {
      document.getElementById("timer__display").innerHTML = Drupal.t('Event has started!');
      if (document.getElementById("social-event-invite-flow-join-virtual-event-as-guest")) {
        document.getElementById("social-event-invite-flow-join-virtual-event-as-guest").classList.remove('visually-hidden');
      }
      if (document.getElementById("social-event-invite-flow-join-virtual-event-as-user")) {
        document.getElementById("social-event-invite-flow-join-virtual-event-as-user").classList.remove('visually-hidden');
      }
      if (document.getElementById("event__image")) {
        document.getElementById("event__image").classList.add('visually-hidden');
      }
    }    
    
  }
}, 1000);      
  
      
    }
  };
})(jQuery, Drupal, drupalSettings);
