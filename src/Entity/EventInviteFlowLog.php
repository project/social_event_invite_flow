<?php

namespace Drupal\social_event_invite_flow\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\social_event\EventEnrollmentInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\social_event_invite_flow\EventInviteFlowLogInterface;

/**
 * Class EventInviteFlowLog.
 *
 * @package Drupal\social_event_invite_flow\Entity
 *
 * @ContentEntityType(
 *   id = "event_invite_flow_log",
 *   label = @Translation("Event Invite Flow Log"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData"
 *   },
 *   base_table = "event_invite_flow_log",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class EventInviteFlowLog extends ContentEntityBase implements EventInviteFlowLogInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
      'has_invitation_sent' => FALSE,
      'has_invitee_enrolled' => FALSE,
      'has_invitee_joined' => FALSE
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->setOwnerId($account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Inviter'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default');

    $fields['event'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Event'))
      ->setSetting('target_type', 'node')
      ->setSetting('handler', 'default');

    $fields['event_enrollment'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Event Enrollment'))
      ->setSetting('target_type', 'event_enrollment')
      ->setSetting('handler', 'default');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['invitation_sent'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Invitation sent at'))
      ->setDescription(t('The time the invitation was sent to the invitee.'));

    $fields['invitee_enrolled'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Invitee enrolled'))
      ->setDescription(t('The time the invitee enrolled to the event.'));

    $fields['invitee_joined'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Invitee joined event'))
      ->setDescription(t('The time that the invitee joined the event.'));

    $fields['invite_flow'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Invite flow type'))
      ->setDescription(t('The used invite flow.'));      

    $fields['invitee_email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Invitee email'))
      ->setDescription(t('The email of the invitee.')); 
      
    $fields['shareable_link'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Shareable link'))
      ->setDescription(t('Is the link shareable?'));    
      
    $fields['has_invitation_sent'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Invitation sent'))
      ->setDescription(t('Was the invitation sent to the user?'));
      
    $fields['has_invitee_enrolled'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Invitee enrolled'))
      ->setDescription(t('Has the invitee enrolled?'));

    $fields['has_invitee_joined'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Invitee joined'))
      ->setDescription(t('Has the invitee joined the event'));     
      
 
    return $fields;

  }

  /**
   * {@inheritdoc}
   */
  public function getEvent() {
    return $this->get('event')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventId() {
    return $this->get('event')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setEvent(NodeInterface $event) {
    $this->setEventId($event->id());
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function setEventId($event) {
    $this->set('event', $event);
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public function getEventEnrollment() {
    return $this->get('event_enrollment')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventEnrollmentId() {
    return $this->get('event_enrollment')->target_id;
  }  

  /**
   * {@inheritdoc}
   */
  public function setEventEnrollment(EventEnrollmentInterface $event_enrollment) {
    $this->setEventEnrollmentId($event_enrollment->id());
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function setEventEnrollmentId($event_enrollment) {
    $this->set('event_enrollment', $event_enrollment);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getInvitationSent() {
    return $this->get('invitation_sent')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInvitationSent($invitation_sent) {
    $this->get('invitation_sent')->setValue($invitation_sent);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getInviteeEnrolled() {
    return $this->get('invitee_enrolled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInviteeEnrolled($invitee_enrolled) {
    $this->get('invitee_enrolled')->setValue($invitee_enrolled);
    return $this;
  }    

  /**
   * {@inheritdoc}
   */
  public function getInviteeJoined() {
    return $this->get('invitee_joined')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInviteeJoined($invitee_joined) {
    $this->get('invitee_enrolled')->setValue($invitee_enrolled);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getInviteFlow() {
    return $this->get('invite_flow')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInviteFlow($invite_flow) {
    $this->get('invite_flow')->setValue($invite_flow);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getInviteeEmail() {
    return $this->get('invitee_email')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInviteeEmail($invitee_email) {
    $this->get('invitee_email')->setValue($invitee_email);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getShareableLink() {
    return $this->get('shareable_link')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setShareableLink($shareable_link) {
    $this->get('shareable_link')->setValue($shareable_link);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getHasInvitationSent() {
    return $this->get('has_invitation_sent')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHasInvitationSent($has_invitation_sent) {
    $this->get('has_invitation_sent')->setValue($has_invitation_sent);
    return $this;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getHasInviteeEnrolled() {
    return $this->get('has_invitee_enrolled')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHasInviteeEnrolled($has_invitee_enrolled) {
    $this->get('has_invitee_enrolled')->setValue($has_invitee_enrolled);
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getHasInviteeJoined() {
    return $this->get('has_invitee_joined')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHasInviteeJoined($has_invitee_joined) {
    $this->get('has_invitee_joined')->setValue($has_invitee_joined);
    return $this;
  }   

}