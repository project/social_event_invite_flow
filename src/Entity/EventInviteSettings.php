<?php

namespace Drupal\social_event_invite_flow\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\social_event_invite_flow\EventInviteSettingsInterface;

/**
 * Defines the event invite settings entity type.
 *
 * @ConfigEntityType(
 *   id = "event_invite_settings",
 *   label = @Translation("Event Invite Settings"),
 *   label_collection = @Translation("Event Invite Settingss"),
 *   label_singular = @Translation("event invite settings"),
 *   label_plural = @Translation("event invite settingss"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event invite settings",
 *     plural = "@count event invite settingss",
 *   ),
 *   config_prefix = "event_invite_settings",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "invite_mode_existing_accounts",
 *     "invite_mode_new_accounts",
 *     "selected_webform_existing_accounts",
 *     "selected_webform_new_accounts",
 *     "access_token",
 *     "enable_shareable_link",
 *     "node"
 *   }
 * )
 */
class EventInviteSettings extends ConfigEntityBase implements EventInviteSettingsInterface {

  /**
   * The event invite settings ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The event invite settings invite mode for existing accounts.
   *
   * @var string
   */
  protected $invite_mode_existing_accounts;

  /**
   * The event invite settings invite mode for new accounts.
   *
   * @var bool
   */
  protected $invite_mode_new_accounts;

  /**
   * The event_invite_settings selected webform for existing accounts.
   *
   * @var string
   */
  protected $selected_webform_existing_accounts;

  /**
   * The event_invite_settings selected webform for new accounts.
   *
   * @var string
   */
  protected $selected_webform_new_accounts;

  /**
   * The event_invite_settings access token.
   *
   * @var string
   */
  protected $access_token; 

  /**
   * The event_invite_settings enable shareable link.
   *
   * @var string
   */
  protected $enable_shareable_link;   

  /**
   * The event_invite_settings node.
   *
   * @var string
   */
  protected $node;  

  /**
   * {@inheritdoc}
   */
  public function getInviteModeExistingAccounts() {
    return $this->invite_mode_existing_accounts;
  }

  /**
   * {@inheritdoc}
   */
  public function setInviteModeExistingAccounts(string $invite_mode_existing_accounts) {
    $this->invite_mode_existing_accounts = $invite_mode_existing_accounts;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getInviteModeNewAccounts() {
    return $this->invite_mode_new_accounts;
  }

  /**
   * {@inheritdoc}
   */
  public function setInviteModeNewAccounts(string $invite_mode_new_accounts) {
    $this->invite_mode_new_accounts = $invite_mode_new_accounts;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectedWebformExistingAccounts() {
    return $this->selected_webform_existing_accounts;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelectedWebformExistingAccounts(string $selected_webform_existing_accounts) {
    $this->selected_webform_existing_accounts = $selected_webform_existing_accounts;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getSelectedWebformNewAccounts() {
    return $this->selected_webform_new_accounts;
  }

  /**
   * {@inheritdoc}
   */
  public function setSelectedWebformNewAccounts(string $selected_webform_new_accounts) {
    $this->selected_webform_new_accounts = $selected_webform_new_accounts;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getAccessToken() {
    return $this->access_token;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessToken(string $access_token) {
    $this->access_token = $access_token;
    return $this;
  }   

  /**
   * {@inheritdoc}
   */
  public function getEnableShareableLink() {
    return $this->enable_shareable_link;
  }

  /**
   * {@inheritdoc}
   */
  public function setEnableShareableLink(bool $enable_shareable_link) {
    $this->enable_shareable_link = $enable_shareable_link;
    return $this;
  }     
  
  /**
   * {@inheritdoc}
   */
  public function getNode() {
    return $this->node;
  }

  /**
   * {@inheritdoc}
   */
  public function setNode(string $node) {
    $this->node = $node;
    return $this;
  }  


}
