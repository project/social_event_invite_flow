<?php

namespace Drupal\social_event_invite_flow\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\social_event_invite_flow\EventInviteMessageInterface;

/**
 * Defines the Event Invite Message entity.
 *
 * @ConfigEntityType(
 *   id = "event_invite_message",
 *   label = @Translation("Event Invite Message"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\social_event_invite_flow\Form\EventInviteMessageForm",
 *       "edit" = "Drupal\social_event_invite_flow\Form\EventInviteMessageForm",
 *       "delete" = "Drupal\social_event_invite_flow\Form\EventInviteMessageDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\social_event_invite_flow\EventInviteMessageHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\social_event_invite_flow\Access\EventInviteMessageAccessControlHandler",
 *   }, 
 *   config_prefix = "event_invite_message",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/node/{node}/event_invite_message/add",
 *     "edit-form" = "/event_invite_message/{event_invite_message}/edit",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "reply_to",
 *     "guests_invite_subject",
 *     "guests_invite_message",
 *     "new_accounts_invite_subject",
 *     "new_accounts_invite_message",
 *     "existing_accounts_invite_subject",
 *     "existing_accounts_invite_message",
 *     "node",
 *     "uuid"
 *   }
 * )
 */
class EventInviteMessage extends ConfigEntityBase implements EventInviteMessageInterface {

  /**
   * The Event Invite Message ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Event Invite Message label.
   *
   * @var string
   */
  protected $label;  

  /**
   * The Event Invite Reply to email.
   *
   * @var string
   */
  protected $reply_to;   

  /**
   * The Guests Invite Subject.
   *
   * @var string
   */
  protected $guests_invite_subject;

  /**
   * The Guests Invite Message.
   *
   * @var string
   */
  protected $guests_invite_message;  

  /**
   * The New accounts Invite Subject.
   *
   * @var string
   */
  protected $new_accounts_invite_subject;  

  /**
   * The New accounts Invite Message.
   *
   * @var string
   */
  protected $new_accounts_invite_message;  

  /**
   * The Existing Accounts Invite Subject.
   *
   * @var string
   */
  protected $existing_accounts_invite_subject;  

  /**
   * The Existing Accounts Invite Message.
   *
   * @var string
   */
  protected $existing_accounts_invite_message;  

  /**
   * The Event Invite Message Node.
   *
   * @var string
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  public function getReplyTo() {
    return $this->reply_to;
  }

  /**
   * {@inheritdoc}
   */
  public function setReplyTo(string $reply_to) {
    $this->reply_to = $reply_to;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGuestsInviteSubject() {
    return $this->guests_invite_subject;
  }

  /**
   * {@inheritdoc}
   */
  public function setGuestsInviteSubject(string $guests_invite_subject) {
    $this->guests_invite_subject = $guests_invite_subject;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGuestsInviteMessage() {
    return $this->guests_invite_message;
  }

  /**
   * {@inheritdoc}
   */
  public function setGuestsInviteMessage(array $guests_invite_message) {
    $this->guests_invite_message = $guests_invite_message;
    return $this;
  }  

  /**
   * {@inheritdoc}
   */
  public function getNewAccountsInviteSubject() {
    return $this->new_accounts_invite_subject;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewAccountsInviteSubject(string $new_accounts_invite_subject) {
    $this->new_accounts_invite_subject = $new_accounts_invite_subject;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNewAccountsInviteMessage() {
    return $this->new_accounts_invite_message;
  }

  /**
   * {@inheritdoc}
   */
  public function setNewAccountsInviteMessage(array $new_accounts_invite_message) {
    $this->new_accounts_invite_message = $new_accounts_invite_message;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExistingAccountsInviteSubject() {
    return $this->existing_accounts_invite_subject;
  }

  /**
   * {@inheritdoc}
   */
  public function setExistingAccountsInviteSubject(string $existing_accounts_invite_subject) {
    $this->existing_accounts_invite_subject = $existing_accounts_invite_subject;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExistingAccountsInviteMessage() {
    return $this->existing_accounts_invite_message;
  }

  /**
   * {@inheritdoc}
   */
  public function setExistingAccountsInviteMessage(array $existing_accounts_invite_message) {
    $this->existing_accounts_invite_message = $existing_accounts_invite_message;
    return $this;
  } 

  /**
   * {@inheritdoc}
   */
  public function getNode() {
    return $this->node;
  }

  /**
   * {@inheritdoc}
   */
  public function setNode(string $node) {
    $this->node = $node;
    return $this;
  }

}
