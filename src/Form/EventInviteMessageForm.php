<?php

namespace Drupal\social_event_invite_flow\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\group\GroupStorageInterface;
use Drupal\Core\Routing;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Class EventInviteMessageForm.
 */
class EventInviteMessageForm extends EntityForm {  

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Get default messages from service
    $invite_flow_service = \Drupal::service('social_event_invite_flow.invite_flow_service');

    $event_invite_message = $this->entity;

    // Get settings
    $settings = $this->config('social_event_invite_flow.settings'); 

    // Make the label group id to avoid dupication
    // Set the entity reference field and attach given group_id

    // Get the group id
    $node_id = \Drupal::routeMatch()->getParameter('node');
 

    if ($this->operation == 'add') {

      $event_invite_message->setNode($node_id);
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');
      $node = $node_storage->load($event_invite_message->getNode());
      $label_default_value = 'Event Invite Messsage' . ' (' . $node->id() . ')';
      $id_default_value = $node->bundle() . '_' . $node->uuid();
      $event_invite_message->set('label', $label_default_value);
      $event_invite_message->set('id', $id_default_value);


      
      if ($defaults = $invite_flow_service->getEventInviteDefaultMessages()) {
        $event_invite_message->setReplyTo($defaults['reply_to']);
        $event_invite_message->setGuestsInviteSubject($defaults['guests_invite_subject']);
        $event_invite_message->setGuestsInviteMessage($defaults['guests_invite_message']);
        $event_invite_message->setNewAccountsInviteSubject($defaults['new_accounts_invite_subject']);
        $event_invite_message->setNewAccountsInviteMessage($defaults['new_accounts_invite_message']);
        $event_invite_message->setExistingAccountsInviteSubject($defaults['existing_accounts_invite_subject']);
        $event_invite_message->setExistingAccountsInviteMessage($defaults['existing_accounts_invite_message']);                
      }            

    }

    // Change page title for the edit operation
    if ($this->operation == 'edit') {
      $node_storage = \Drupal::entityTypeManager()->getStorage('node');
      $node = $node_storage->load($event_invite_message->getNode());
    }



    /*

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $event_invite_message->label(),
      '#required' => TRUE,
      '#disabled' => TRUE,  
      '#attributes' => ['class' => ['visually-hidden']]    
    ];
    
    */




    
    /*

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $event_invite_message->id(),
      '#machine_name' => [
        'exists' => '\Drupal\social_event_invite_flow\Entity\EventInviteMessage::load',
      ],      
      '#disabled' => !$event_invite_message->isNew(),
      //'#disabled' => TRUE,
    ];

    */

    $invite_flow_service->addFieldTokens($form);


    $form['reply_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply to'),
      '#maxlength' => 255,
      '#default_value' => $event_invite_message->getReplyTo(),
     ];

    $form['guests_invite_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invite subject for guests'),
      '#maxlength' => 255,
      '#default_value' => $event_invite_message->getGuestsInviteSubject(),
      '#required' => TRUE,
    ];

 
    $form['guests_invite_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Invite message for guests'),
      '#default_value' => $event_invite_message->getGuestsInviteMessage()['value'],
      '#required' => TRUE,
      '#format' => $settings->get('selected_format'),
      '#allowed_formats' => [
        $settings->get('selected_format')
      ]
    ];

    $form['new_accounts_invite_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invite subject for new accounts'),
      '#maxlength' => 255,
      '#default_value' => $event_invite_message->getNewAccountsInviteSubject(),
      '#required' => TRUE,
    ];

 
    $form['new_accounts_invite_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Invite message for new accounts'),
      '#default_value' => $event_invite_message->getNewAccountsInviteMessage()['value'],
      '#required' => TRUE,
      '#format' => $settings->get('selected_format'),
      '#allowed_formats' => [
        $settings->get('selected_format')
      ]
    ];  
    
    $form['existing_accounts_invite_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Invite subject for existing accounts'),
      '#maxlength' => 255,
      '#default_value' => $event_invite_message->getExistingAccountsInviteSubject(),
      '#required' => TRUE,
    ];

 
    $form['existing_accounts_invite_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Invite message for existing accounts'),
      '#default_value' => $event_invite_message->getExistingAccountsInviteMessage()['value'],
      '#required' => TRUE,
      '#format' => $settings->get('selected_format'),
      '#allowed_formats' => [
        $settings->get('selected_format')
      ]
    ];
    
    /*

    $form['available_tokens'] = array(
      '#type' => 'details',
      '#title' => t('Available Tokens'),
      '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    );

    $suppported_tokens = array('site','user','group');
    
    $available_field_tokens = \Drupal::service('group_welcome_message.available_fields');
    $whitelist = $available_field_tokens->getAvailableFields();

    $options = [
      'show_restricted' => TRUE,
      'show_nested' => TRUE,
      'global_types' => FALSE,
      'whitelist' => $whitelist
    ];  

    $form['available_tokens']['#access'] = $settings->get('show_token_info');

    $form['available_tokens']['tokens'] = \Drupal::service('group_welcome_message.tree_builder')
      ->buildRenderable($suppported_tokens,$options);

    */

    $form['node'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#default_value' => $node,
      '#title' => $this->t('Node'),
      '#disabled' => TRUE
    ];


    $form['#attached']['library'][] = 'social_event_invite_flow/flow_clipboard';
    


    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Get Allowed Tokens.
    $available_field_tokens = \Drupal::service('social_event_invite_flow.invite_flow_service');
    $whitelist = $available_field_tokens->getAvailableFields();

    \Drupal::logger('debug')->debug('<pre><code>' . print_r($whitelist, TRUE) . '</code></pre>');

    // Validate Guests Invite subject
    $tokens_present = preg_match_all("#\[(.*?)\]#", $form_state->getValue('guests_invite_subject'), $matches);
    if ($tokens_present) {

      $found_tokens = $matches[0];
      $wrong_tokens = array_diff($found_tokens,$whitelist);

      if (count($wrong_tokens) > 0) {
        $form_state->setErrorByName('guests_invite_subject', $this->t('Illegal Tokens found in guests invite subject.'));
      }     

    }

    // Validate Guests Invite message
    $tokens_present = preg_match_all("#\[(.*?)\]#", $form_state->getValue('guests_invite_message')['value'], $matches);
    if ($tokens_present) {
    
      $found_tokens = $matches[0];
      $wrong_tokens = array_diff($found_tokens,$whitelist);
    
      if (count($wrong_tokens) > 0) {
        $form_state->setErrorByName('guests_invite_message', $this->t('Illegal Tokens found in guests invite message.'));
      }     
    
    }

    // Validate Guests Invite subject
    $tokens_present = preg_match_all("#\[(.*?)\]#", $form_state->getValue('new_accounts_invite_subject'), $matches);
    if ($tokens_present) {

      $found_tokens = $matches[0];
      $wrong_tokens = array_diff($found_tokens,$whitelist);

      if (count($wrong_tokens) > 0) {
        $form_state->setErrorByName('new_accounts_invite_subject', $this->t('Illegal Tokens found in new accounts invite subject.'));
      }     

    }

    // Validate Guests Invite message
    $tokens_present = preg_match_all("#\[(.*?)\]#", $form_state->getValue('new_accounts_invite_message')['value'], $matches);
    if ($tokens_present) {
    
      $found_tokens = $matches[0];
      $wrong_tokens = array_diff($found_tokens,$whitelist);
    
      if (count($wrong_tokens) > 0) {
        $form_state->setErrorByName('new_accounts_invite_message', $this->t('Illegal Tokens found in new accounts invite message.'));
      }     
    
    }    

    // Validate Guests Invite subject
    $tokens_present = preg_match_all("#\[(.*?)\]#", $form_state->getValue('existing_accounts_invite_subject'), $matches);
    if ($tokens_present) {

      $found_tokens = $matches[0];
      $wrong_tokens = array_diff($found_tokens,$whitelist);

      if (count($wrong_tokens) > 0) {
        $form_state->setErrorByName('existing_accounts_invite_subject', $this->t('Illegal Tokens found in existing accounts invite subject.'));
      }     

    }

    // Validate Guests Invite message
    $tokens_present = preg_match_all("#\[(.*?)\]#", $form_state->getValue('existing_accounts_invite_message')['value'], $matches);
    if ($tokens_present) {
    
      $found_tokens = $matches[0];
      $wrong_tokens = array_diff($found_tokens,$whitelist);
    
      if (count($wrong_tokens) > 0) {
        $form_state->setErrorByName('existing_accounts_invite_message', $this->t('Illegal Tokens found in existing accounts invite message.'));
      }     
    
    }       



  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $event_invite_message = $this->entity;

    $status = $event_invite_message->save();

    switch ($status) {
      case SAVED_NEW:           
        $this->messenger()->addMessage($this->t('Created %label.', [
          '%label' => $event_invite_message->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved %label.', [
          '%label' => $event_invite_message->label(),
        ]));
    }

    /*

    // Decide where to redirect based on module install
    $moduleHandler = \Drupal::service('module_handler');
    // If social distro installed redirect to memberhsip view
    // otherwise the members view of the group module.
    $redirect_route_name = 'view.group_members.page_1';    
    if ($moduleHandler->moduleExists('social_group')) {
      $redirect_route_name = 'view.group_manage_members.page_group_manage_members';
    }

    if ($status != SAVED_NEW) {  

      $url = Url::fromRoute($redirect_route_name,['group' => $group_welcome_message->getGroup()]);
      $form_state->setRedirectUrl($url);

    }

    */

  }

}
