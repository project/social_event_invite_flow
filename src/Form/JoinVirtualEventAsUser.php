<?php

namespace Drupal\social_event_invite_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Provides a Social event invite flow form.
 */
class JoinVirtualEventAsUser extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_event_invite_flow_join_virtual_event_as_user';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL) {


    $user_display_name = \Drupal::currentUser()->getDisplayName();

    $form['#attributes'] = [
      'class' => ['visually-hidden']
    ];


    $form['fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Join Meeting now'),
      '#collapsed' => FALSE,
    ];

    $form['fieldset']['event_id'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ];

    $form['fieldset']['name'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('Hello @user!',['@user' => $user_display_name]),
    ];

    $form['fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['fieldset']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('JOIN MEETING'),
      '#attributes' => [
        'class' => [
          'btn-accent',
          'button',
          'btn',
          'btn-lg',
          'waves-effect',
          'waves-btn',
          'waves-light',
        ]
      ]
    ];

    /** @var \Drupal\data_policy\DataPolicyConsentManagerInterface $data_policy_manager */
    $data_policy_manager = \Drupal::service('data_policy.manager');

    if (!$data_policy_manager->isDataPolicy()) {
      return;
    }

    if (\Drupal::routeMatch()->getRouteName() === 'user.admin_create') {
      return;
    }

    $data_policy_manager->addCheckbox($form);

    return $form;

  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $event_id = $form_state->getValue('event_id');
    $name = \Drupal::currentUser()->getDisplayName();
    $event_invite_flow_service = \Drupal::service('social_event_invite_flow.invite_flow_service');

    $node = \Drupal::entityTypeManager()->getStorage('node')->load($event_id);

    $log_data['event'] = $node;
    
    $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
    $virtualEventsCommon = \Drupal::service('virtual_events.common');
    $event_id = $node->bundle() . '_' . $node->uuid();

    $event = $virtualEventsCommon->getEventById($event_id);

    if ($event) {
      // Check if meeting is not active,
      // recreate it before showing the join url
      $event = $event->reCreate();

      $entity = $event->getEntity();
      $enabled_event_source = $event->getEnabledSourceKey();
      
      $event_config = $event->getVirtualEventsConfig($enabled_event_source);
      $source_config = $event_config->getSourceConfig($enabled_event_source);
     
      $source_data = $event->getSourceData();
      $eventSourcePlugin = $event->getEventSourcePlugin();
      if(!isset($source_config["data"]["key_type"])) {
        $error_message = $this->t("Couldn't create meeting! please contact system administrator.");      
      }

      $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
      $keys = $keyPlugin->getKeys($source_config);

      $apiUrl = $keys["url"];
      $secretKey = $keys["secretKey"];
      $bbb = new VirtualEventBBB($secretKey, $apiUrl);

      // Check access for current entity, if user can update
      // then we can consider the user as moderator,
      // otherwise we consider the user as normal attendee.
      if ($entity->access('update')) {
        $joinMeetingParams = new JoinMeetingParameters($event->id(), $name, $source_data["settings"]["moderatorPW"]);
      }
      elseif ($entity->access('view')) {
        $joinMeetingParams = new JoinMeetingParameters($event->id(), $name, $source_data["settings"]["attendeePW"]);
      }

      try {
        $joinMeetingParams->setRedirect(TRUE);
        $joinMeetingUrl = $bbb->getJoinMeetingURL($joinMeetingParams);
        $form_state->setResponse(new TrustedRedirectResponse($joinMeetingUrl));

        // Log that user has joioned event
        $log_data['invitee_email'] = \Drupal::currentUser()->getEmail();
        $event_invite_flow_service->createInviteFlowLogEntry('joined', $log_data);

      }
      catch (\RuntimeException $exception) {
        $this->getLogger('social_event_invite_flow')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
      catch (Exception $exception) {
        $this->getLogger('social_event_invite_flow')->warning($exception->getMessage());
        $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
        $this->messenger()->addError($error_message);
      }
      
    }

  }

}
