<?php

namespace Drupal\social_event_invite_flow\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class EnrollInviteForm.
 */
class EventInviteFlowSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'event_invite_flow_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //$social_event_config = $this->configFactory->getEditable('social_event_invite_flow.settings');
    $social_event_config = $this->config('social_event_invite_flow.settings');

    $invite_flow_service = \Drupal::service('social_event_invite_flow.invite_flow_service');

    $invite_mode_existing_accounts_options = $invite_flow_service->getInviteModeExistingAccountsOptions();
    $invite_mode_new_accounts_options = $invite_flow_service->getInviteModeNewAccountsOptions();
 
    $filter_options = [];

    $available_filters = filter_formats();

    foreach($available_filters as $id => $filter) {
      $filter_options[$id] = $filter->label();
    }

    // Get all webforms
    $webforms = $invite_flow_service->getAllWebforms();

    // Add an introduction text to explain what can be done here.
    $form['introduction']['warning'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('This is an extenstion to the existing event invite settings. Here you can administer the invite messages for your invitees. You can have different messages for users with accounts, users forced to register for an account and guests with no account at all.'),
    ];

    $form['invite_mode_setting'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Invite Mode Settings')
    ];

    $form['invite_mode_setting']['invite_mode_existing_accounts'] = [
      '#type' => 'radios',
      '#title' => $this->t('Invite mode for existing accounts'),
      '#options' => $invite_mode_existing_accounts_options,
      '#default_value' => $social_event_config->get('invite_mode_existing_accounts'),
      '#required' => TRUE,
    ];

    $form['invite_mode_setting']['webforms_existing_accounts'] = [
      '#type' => 'multiselect',
      '#options' => $webforms,
      '#default_value' => $social_event_config->get('webforms_existing_accounts'),
      '#required' => TRUE,
    ];        

    $form['invite_mode_setting']['invite_mode_new_accounts'] = [
      '#type' => 'radios',
      '#title' => $this->t('Invite mode for new accounts'),
      '#options' => $invite_mode_new_accounts_options,
      '#default_value' => $social_event_config->get('invite_mode_new_accounts'),
      '#required' => TRUE,
    ];    

    $form['invite_mode_setting']['webforms_new_accounts'] = [
      '#type' => 'multiselect',
      '#options' => $webforms,
      '#default_value' => $social_event_config->get('webforms_new_accounts'),
      '#required' => TRUE,
    ];        

    
    $form['invite_messages'] = [
      '#type' => 'details',
      '#title' => $this->t('Invite Emails'),
      '#open' => TRUE
    ];

    $form['invite_messages']['reply_to'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reply to'),
      '#default_value' => $social_event_config->get('reply_to'),
      '#required' => FALSE,
    ];    

    $form['invite_messages']['selected_format'] = [
      '#title' => $this->t('Message format'),
      '#type' => 'select',
      '#options' => $filter_options,
      '#default_value' => $social_event_config->get('selected_format'),
    ];
      
    $form['invite_messages']['guests_invite_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject for guests'),
      '#default_value' => $social_event_config->get('guests_invite_subject'),
      '#required' => TRUE,
    ];

    $form['invite_messages']['guests_invite_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message for guests'),
      '#default_value' => $social_event_config->get('guests_invite_message'),
      '#required' => TRUE,
    ];

    $form['invite_messages']['new_accounts_invite_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject for users with new accounts'),
      '#default_value' => $social_event_config->get('new_accounts_invite_subject'),
      '#required' => TRUE,
    ];

    $form['invite_messages']['new_accounts_invite_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message for users with new accounts'),
      '#default_value' => $social_event_config->get('new_accounts_invite_message'),
      '#required' => TRUE,
    ];   
    
    $form['invite_messages']['existing_accounts_invite_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject for users with existing accounts'),
      '#default_value' => $social_event_config->get('existing_accounts_invite_subject'),
      '#required' => TRUE,
    ];

    $form['invite_messages']['existing_accounts_invite_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message for users with existing accounts'),
      '#default_value' => $social_event_config->get('existing_accounts_invite_message'),
      '#required' => TRUE,
    ];    

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#button_level' => 'raised',
      '#value' => $this->t('Save configuration'),
    ];

    $form['#attached']['library'][] = 'social_event_invite_flow/flow_design';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {    
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('social_event_invite_flow.settings');
    $config->set('invite_mode_existing_accounts', $form_state->getValue('invite_mode_existing_accounts'));
    $config->set('invite_mode_new_accounts', $form_state->getValue('invite_mode_new_accounts'));
    $config->set('webforms_existing_accounts', $form_state->getValue('webforms_existing_accounts'));
    $config->set('webforms_new_accounts', $form_state->getValue('webforms_new_accounts'));
    $config->set('reply_to', $form_state->getValue('reply_to'));
    $config->set('selected_format', $form_state->getValue('selected_format'));
    $config->set('guests_invite_subject', $form_state->getValue('guests_invite_subject'));
    $config->set('guests_invite_message', $form_state->getValue('guests_invite_message'));
    $config->set('new_accounts_invite_subject', $form_state->getValue('new_accounts_invite_subject'));
    $config->set('new_accounts_invite_message', $form_state->getValue('new_accounts_invite_message'));
    $config->set('existing_accounts_invite_subject', $form_state->getValue('existing_accounts_invite_subject'));
    $config->set('existing_accounts_invite_message', $form_state->getValue('existing_accounts_invite_message'));
    $config->save();
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method. 
   */
  protected function getEditableConfigNames() {
    return [
      'social_event_invite_flow.settings'
    ];
  }
}
