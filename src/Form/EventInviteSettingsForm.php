<?php

namespace Drupal\social_event_invite_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\social_event_invite_flow\Service\EventInviteFlowService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;

/**
 * Class EventInviteSettingsForm.
 */
class EventInviteSettingsForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The current group from route.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $group;

  /**
   * The invite flow service.
   * 
   * @var \Drupal\social_event_invite_flow\Service\EventInviteFlowService
   */
  protected EventInviteFlowService $eventInviteFlowService;  

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->routeMatch = $instance->getRouteMatch();
    $instance->eventInviteFlowService = $container->get('social_event_invite_flow.invite_flow_service');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'event_invite_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->routeMatch->getParameter('node');

    if (!$node instanceof NodeInterface) {
      $node = $this->entityTypeManager->getStorage('node')->load($node);
    }

    

    

    // Get destination
    $destination = \Drupal::destination()->getAsArray();

    $form['event_invite_settings'] = [
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      '#title' => $this->t('Event Invite Settings')
    ];

    if (!$this->eventInviteFlowService->isInviteFlowSettingsPresent()) {
      $form['event_invite_settings']['set_invite_flow_settings']['link'] = [
        '#type' => 'link',
        '#title' => $this->t('Please configure settings first'),
        '#url' => Url::fromRoute('social_event_invite_flow.settings',[], ['query' => $destination]),
      ];
      return $form;      
    }

    // Build our flex box container
    $form['event_invite_settings']['invite_container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['invite--flow']],
    ]; 

    // Build our flex box left column
    $form['event_invite_settings']['invite_container']['left'] = [
      '#type' => 'container'
    ]; 

    // Build our flex box right column
    $form['event_invite_settings']['invite_container']['right'] = [
      '#type' => 'container'
    ];  


    $default_lang = \Drupal::service('language_manager')->getDefaultLanguage();
    $curr_user = \Drupal::currentUser()->id();
    $user_object = $this->entityTypeManager->getStorage('user')->load($curr_user);
    $pref_lang = $user_object->getPreferredLangcode(); 

    $form['event_invite_settings']['invite_container']['left']['invite_mode_existing_accounts'] = [
      '#type' => 'radios',
      '#title' => $this->t('Invite mode for existing accounts'),
      '#options' => $this->eventInviteFlowService->getInviteModeExistingAccountsOptions(),
      '#required' => TRUE,
      '#attributes' => [
        'title' => t("Here you can choose whether to allow your users to share the invitation link with others."),
      ],
      '#default_value' => $this->eventInviteFlowService->getInviteSettingsDefaultData()['invite_mode_existing_accounts'],       
    ];

    $selected_webform_existing_accounts_default = $this->eventInviteFlowService->getInviteSettingsDefaultData()['selected_webform_existing_accounts'];
    
    $form['event_invite_settings']['invite_container']['left']['selected_webform_existing_accounts'] = [
      '#type' => 'select',
      '#title' => $this->t('Select webform'),
      '#options' => $this->eventInviteFlowService->getWebformsExistingAccountsOptions(),
      '#states' => [
        'visible' => [
          ':input[name="invite_mode_existing_accounts"]' => [
            'value' => 'webform_existing_accounts',
          ],
        ],
      ],
      '#default_value' => isset($selected_webform_existing_accounts_default) ? $selected_webform_existing_accounts_default : ''
    ];

    $form['event_invite_settings']['invite_container']['right']['invite_mode_new_accounts'] = [
      '#type' => 'radios',
      '#title' => $this->t('Invite mode for new accounts'),
      '#options' => $this->eventInviteFlowService->getInviteModeNewAccountsOptions(),
      '#required' => TRUE,
      '#attributes' => [
        'title' => t("Here you can choose whether to allow your users to share the invitation link with others."),
      ],
      '#default_value' => $this->eventInviteFlowService->getInviteSettingsDefaultData()['invite_mode_new_accounts']      
    ];

    $selected_webform_new_accounts = $this->eventInviteFlowService->getInviteSettingsDefaultData()['selected_webform_new_accounts'];

    $form['event_invite_settings']['invite_container']['right']['selected_webform_new_accounts'] = [
      '#type' => 'select',
      '#title' => $this->t('Select webform'),
      '#options' => $this->eventInviteFlowService->getWebformsNewAccountsOptions(),
      '#states' => [
        'visible' => [
          ':input[name="invite_mode_new_accounts"]' => [
            'value' => 'webform_new_accounts'
          ],
        ],
      ],
      '#default_value' => isset($selected_webform_new_accounts) ? $selected_webform_new_accounts : ''
    ];

    $options = [
      'dialogClass' => 'event-invite-message',
      'width' => '800',
    ];
    
    if ($this->eventInviteFlowService->getEventInviteMessages()) {
      $event_invite_message_id = $node->bundle() . '_' . $node->uuid();
      $url = Url::fromRoute('entity.event_invite_message.edit_form', [
        'event_invite_message' => $event_invite_message_id],['query' => $destination]);
      $event_message_link_title = $this->t('Customize Invite Messages');
    }
    else {
      $node_id = $node->id();
      $url = Url::fromRoute('entity.event_invite_message.add_form', ['node' => $node_id],['query' => $destination]);
      $event_message_link_title = $this->t('Customize Invite Messages');
    }

    $form['event_message_actions'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['']],
    ];

    $event_message_link = [
      'manage_invite_message' => [
        '#type' => 'link',
        '#title' => $event_message_link_title,
        '#url' => $url,
        '#attributes' => [
          'class' => ['use-ajax', 'button', 'btn','btn-primary', 'btn-block'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => json_encode($options)
        ],
      ]
    ];

    $form['event_message_actions']['event_message_link'] = $event_message_link;

    $form['#attached']['library'][] = 'social_event_invite_flow/flow_design';    

    return $form;


  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }


}
