<?php

namespace Drupal\social_event_invite_flow\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\virtual_event_bbb\VirtualEventBBB;
use BigBlueButton\Parameters\JoinMeetingParameters;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;

/**
 * Provides a Social event invite flow form.
 */
class JoinVirtualEventAsGuest extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_event_invite_flow_join_virtual_event_as_guest';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL) {
   

    // First let's check if we have an existing enrollment for
    // the current user.
    $event_invite_flow_service = \Drupal::service('social_event_invite_flow.invite_flow_service');
    $sid = $event_invite_flow_service->getSessionIDForUnauthenticatedUsers();

    // Check if we need the email field for the guest
    //$event_shareable = $event_invite_flow_service->isEventShareable($nid);

    // Get parameter invitee
    $invitee = \Drupal::request()->query->get('invitee');

    // Get parameter token
    $token = \Drupal::request()->query->get('token');

    // Get parameter access_token
    $access_token = \Drupal::request()->query->get('access_token');

    if (isset($access_token) && !empty($access_token)) {
      // Check if we already have an enrolled anon. user that holds an access_token
      $enrollment = FALSE;
    }
    else {
      if (isset($token) && !empty($token)) {
        // Check if we already have an enrolled anon. user that holds an token
        $enrollment = $event_invite_flow_service->enrollmentExistsByToken($token, $nid);
      }
    }

    if (isset($invitee)) {
      $invitee_email = $event_invite_flow_service->getDecodedEmailString($invitee);
    }
    else {
      $invitee_email = '';
    }
   
    $form['#attributes'] = [
      'class' => ['visually-hidden']
    ];

    $form['fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Join Meeting now'),
      '#collapsed' => FALSE,
    ];

    // Get destination
    $destination = \Drupal::destination()->getAsArray();

    $login_url = Url::fromRoute('user.login', [], ['query' => $destination]);

    $form['fieldset']['having_account'] = [
      '#type' => 'link',
      '#title' => $this->t('Do you have an account? Click here to join the meeting.'),
      '#url' => $login_url,
      '#weight' => -1
    ];

    $form['fieldset']['event_id'] = [
      '#type' => 'hidden',
      '#value' => $nid,
    ]; 
    
    if ($enrollment) {
      $form['fieldset']['name'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->t('Hello @user!',['@user' => $enrollment->field_first_name->value]),
      ];
    }
    else {

      if (!isset($access_token)) {

        if (isset($invitee_email) && !empty($invitee_email)) {
          $email_disabled = TRUE;
        }
        else {
          $email_disabled = FALSE;
        }

        $form['fieldset']['email'] = [
          '#type' => 'email',
          '#title' => $this->t('Email'),
          '#required' => TRUE,
          '#default_value' => $invitee_email,
          '#disabled' => $email_disabled
        ];

      }

      $form['fieldset']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#required' => TRUE,
      ];

    }
    
    $form['fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['fieldset']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('JOIN MEETING'),
      '#attributes' => [
        'class' => [
          'btn-accent',
          'button',
          'btn',
          'btn-lg',
          'waves-effect',
          'waves-btn',
          'waves-light',
        ]
      ]
    ];

    // Prevent any caching
    $form['#cache'] = ['max-age' => 0];

    /** @var \Drupal\data_policy\DataPolicyConsentManagerInterface $data_policy_manager */
    $data_policy_manager = \Drupal::service('data_policy.manager');

    if (!$data_policy_manager->isDataPolicy()) {
      return;
    }
    
    $data_policy_manager->addCheckbox($form);

    return $form;
  
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Get the event id (node)
    $event_id = $form_state->getValue('event_id');    

    $event_invite_flow_service = \Drupal::service('social_event_invite_flow.invite_flow_service');

    $email = $form_state->getValue('email');

    if (isset($email)) {

      if ($event_invite_flow_service->checkIfEmailHasAccount($email)) {
        $form_state->setErrorByName('email', $this->t('The entered email belongs to an existing account. Please use the link above to join the meeting!'));        
      }
      else {
        if (!$event_invite_flow_service->checkIfUnauthenticatedUserHasEnrolled($event_id, $email)) {
          $form_state->setErrorByName('email', $this->t('The email address you entered is not the same one to which the invitation to our event was sent. If you already have an account please login to join!'));
        }  
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $enrollment = FALSE;

     // Get parameter token
    $token = \Drupal::request()->query->get('token');

    // Get parameter access_token
    $access_token = \Drupal::request()->query->get('access_token');    

    // Get the event id (node)
    $event_id = $form_state->getValue('event_id');

    // Get the name from the form
    $name = $form_state->getValue('name');    

    // First let's check if we have an existing enrollment for
    // the current user.
    $event_invite_flow_service = \Drupal::service('social_event_invite_flow.invite_flow_service');
    $sid = $event_invite_flow_service->getSessionIDForUnauthenticatedUsers();
   
    // Check if we need the email field for the guest to enroll
    if (isset($access_token) && !empty($access_token)) {
      // Check if we already have an enrolled anon. user that holds an access_token
      $enrollment = FALSE;
    }
    else {
      if (isset($token) && !empty($token)) {
        // Check if we already have an enrolled anon. user that holds an token
        $enrollment = $event_invite_flow_service->enrollmentExistsByToken($token, $event_id);
      }
    }


    if ($enrollment) {
      $name = $enrollment->field_first_name->value;
      $log_data['invitee_email'] = $enrollment->field_email->value; 
    }
    else {      
      if (!isset($access_token)) {
        $email = $form_state->getValue('email');
        // Enroll guest when not enrolled with an email address
        $anon_enrolled = $event_invite_flow_service->enrollGuestInEventWithEmail($event_id, $name, $email);
        //$log_data['event_enrollment'] = $anon_enrolled;
        $log_data['invitee_email'] = $anon_enrolled->field_email->value;
        \Drupal::logger('debug')->debug('emdail: ' . $log_data['invitee_email']);
      }
      else {
        // Enroll guest when not enrolled without an email address
        $anon_enrolled = $event_invite_flow_service->enrollGuestInEvent($event_id, $name);
        //$log_data['event_enrollment'] = $anon_enrolled;
      }
    }

    if (isset($name)) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($event_id);

      $log_data['event'] = $event_id;
    
      $BBBKeyPluginManager = \Drupal::service('plugin.manager.bbbkey_plugin');
      $virtualEventsCommon = \Drupal::service('virtual_events.common');
      $event_id = $node->bundle() . '_' . $node->uuid();
  
  
      $event = $virtualEventsCommon->getEventById($event_id);
  
      if ($event) {
        // Check if meeting is not active,
        // recreate it before showing the join url
        $event = $event->reCreate();
  
        $entity = $event->getEntity();
        $enabled_event_source = $event->getEnabledSourceKey();
        
        $event_config = $event->getVirtualEventsConfig($enabled_event_source);
        $source_config = $event_config->getSourceConfig($enabled_event_source);
       
        $source_data = $event->getSourceData();
        $eventSourcePlugin = $event->getEventSourcePlugin();
        if(!isset($source_config["data"]["key_type"])) {
          $error_message = $this->t("Couldn't create meeting! please contact system administrator.");      
        }
  
        $keyPlugin = $BBBKeyPluginManager->createInstance($source_config["data"]["key_type"]);
        $keys = $keyPlugin->getKeys($source_config);
  
        $apiUrl = $keys["url"];
        $secretKey = $keys["secretKey"];
        $bbb = new VirtualEventBBB($secretKey, $apiUrl);
  
        // Check access for current entity, if user can update
        // then we can consider the user as moderator,
        // otherwise we consider the user as normal attendee.
        $joinMeetingParams = new JoinMeetingParameters($event->id(), $name, $source_data["settings"]["attendeePW"]);        

        try {
          $joinMeetingParams->setRedirect(TRUE);
          $joinMeetingUrl = $bbb->getJoinMeetingURL($joinMeetingParams);
          $form_state->setResponse(new TrustedRedirectResponse($joinMeetingUrl));  
          //$event_invite_flow_service->createInviteFlowLogEntry('joined', $log_data);       
        }
        catch (\RuntimeException $exception) {
          $this->getLogger('social_event_invite_flow')->warning($exception->getMessage());
          $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
          $this->messenger()->addError($error_message);
        }
        catch (Exception $exception) {
          $this->getLogger('social_event_invite_flow')->warning($exception->getMessage());
          $error_message = $this->t("Couldn't get meeting join link! please contact system administrator.");
          $this->messenger()->addError($error_message);
        }

      }
    }
  }
}
