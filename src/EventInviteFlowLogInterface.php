<?php

namespace Drupal\social_event_invite_flow;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\social_event\EventEnrollmentInterface;
use Drupal\node\NodeInterface;

/**
 * Provides an interface for defining Event Invite Flow Log entities.
 */
interface EventInviteFlowLogInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Get the event node.
   */
  public function getEvent();

  /**
   * Get the event node id.
   */
  public function getEventId();

  /**
   * Set the event node.
   */
  public function setEvent(NodeInterface $event);

  /**
   * Set the event node id.
   */
  public function setEventId($event);

  /**
   * Get the event enrollment entity.
   */
  public function getEventEnrollment();

  /**
   * Get the event enrollment id.
   */
  public function getEventEnrollmentId();

  /**
   * Set the event enrollment.
   */
  public function setEventEnrollment(EventEnrollmentInterface $event_enrollment);

  /**
   * Set the event enrollment id.
   */
  public function setEventEnrollmentId($event_enrollment);

  /**
   * Get Invitation sent timestamp
   */
  public function getInvitationSent();

  /**
   * Set the invation sent timestamp.
   */
  public function setInvitationSent($invitation_sent);

  /**
   * Get invitee enrolled timestamp.
   */
  public function getInviteeEnrolled();

  /**
   * Set invitee enrolled timestamp.
   */
  public function setInviteeEnrolled($invitee_enrolled);

  /**
   * Get invitee joined timestamp.
   */
  public function getInviteeJoined();

  /**
   * Set invitee joined
   */
  public function setInviteeJoined($invitee_joined);

  /**
   * Get the invite flow.
   */
  public function getInviteFlow();

  /**
   * Set the invite flow.
   */
  public function setInviteFlow($invite_flow);

  /**
   * Get Invitee Email.
   */
  public function getInviteeEmail();

  /**
   * Set invitee email.
   */
  public function setInviteeEmail($invitee_email);

  /**
   * Get shareable link
   */
  public function getShareableLink();

  /**
   * Set shareable link
   */
  public function setShareableLink($shareable_link);


  /**
   * Get has Invitation sent
   */
  public function getHasInvitationSent();

  /**
   * Set has invitation sent
   */
  public function setHasInvitationSent($has_invitation_sent);
  
  /**
   * Get has invitee enrolled
   */
  public function getHasInviteeEnrolled();

  /**
   * Set has invitee enrolled
   */
  public function setHasInviteeEnrolled($has_invitee_enrolled);

  /**
   * Get Has Invitee joined
   */
  public function getHasInviteeJoined();

  /**
   * Set invitee joined
   */
  public function setHasInviteeJoined($has_invitee_joined);


}
