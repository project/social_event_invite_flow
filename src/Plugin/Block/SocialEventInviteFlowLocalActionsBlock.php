<?php

namespace Drupal\social_event_invite_flow\Plugin\Block;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\social_event_invite\SocialEventInviteAccessHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'SocialEventInviteFlowLocalActionsBlock' block.
 *
 * @Block(
 *  id = "social_event_invite_flow_block",
 *  admin_label = @Translation("Social Event Invite Flow block"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"), required = FALSE)
 *   }
 * )
 */
class SocialEventInviteFlowLocalActionsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The event invite access helper.
   *
   * @var \Drupal\social_event_invite\SocialEventInviteAccessHelper
   */
  protected $accessHelper;

  /**
   * EventAddBlock constructor.
   *
   * @param array $configuration
   *   The given configuration.
   * @param string $plugin_id
   *   The given plugin id.
   * @param mixed $plugin_definition
   *   The given plugin definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   * @param \Drupal\social_event_invite\SocialEventInviteAccessHelper $accessHelper
   *   The event invite access helper.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $routeMatch, SocialEventInviteAccessHelper $accessHelper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->routeMatch = $routeMatch;
    $this->accessHelper = $accessHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('social_event_invite.access_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    try {
      return $this->accessHelper->eventFeatureAccess();
    }
    catch (InvalidPluginDefinitionException $e) {
      return AccessResult::neutral();
    }
    catch (PluginNotFoundException $e) {
      return AccessResult::neutral();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Get current node so we can build correct links.
    $event = $this->getContextValue('node');
    if ($event instanceof NodeInterface) {
    
    
	    $links['enroll_users'] = [
  	    '#title' => $this->t('Enroll users'),
        '#type' => 'link',
        '#url' => Url::fromRoute('social_event_invite_flow.invite_simple', ['node' => $event->id()]),
        '#attributes' => [
          'class' => [
            'btn',
            'btn-default',
            'btn-raised',
            'waves-effect',
          ],
        ]
      ];    

	    $links['import_users'] = [
  	    '#title' => $this->t('Bulk enroll users'),
        '#type' => 'link',
        '#url' => Url::fromRoute('social_event_invite_flow.invite_upload', ['node' => $event->id()]),
        '#attributes' => [
          'class' => [
            'btn',
            'btn-default',
            'btn-raised',
            'waves-effect',
          ],
        ]
      ];          
    

      $build['content'] = $links;
      $build['#cache'] = [
        'keys' => ['social_event_invite_flow_block', 'node', $event->id()],
        'contexts' => ['user'],
      ];
    }
    return $build;
  }

}
