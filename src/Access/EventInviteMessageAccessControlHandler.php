<?php

namespace Drupal\social_event_invite_flow\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;



class EventInviteMessageAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {

    $access = AccessResult::forbidden();


    if (social_event_manager_or_organizer()) {
      $access = AccessResult::allowed();
    }
    else {
      $access = parent::access($entity, $operation, $account, $return_as_object);
    }
    
    return $access;

  }

   /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    $access = AccessResult::forbidden();


    if (social_event_manager_or_organizer()) {
      $access = AccessResult::allowed();
    }
    else {
      $access = parent::checkAccess($entity, $operation, $account);
    }
    
    return $access;

  }

  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {

    $access = AccessResult::forbidden();

    if (social_event_manager_or_organizer()) {
      $access = AccessResult::allowed();
    }
    else {
      $access = parent::checkCreateAccess($account, $context, $entity_bundle);
    }
    
    return $access;

  }  




}
