<?php

namespace Drupal\social_event_invite_flow\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to for translating event invite messages.
 */
class EventInviteMessageTranslateConfigAccessCheck implements AccessInterface {

  /**
   * Checks access to the event invite message translate routes.
   */
  public function access(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, "translate event invite messages");
  }

}
