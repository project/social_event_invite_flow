<?php

namespace Drupal\social_event_invite_flow;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Event Invite Message entities.
 */
interface EventInviteMessageInterface extends ConfigEntityInterface {

  /**
   * Gets the Reply to email
   */
  public function getReplyTo(); 

  /**
   * Sets the Reply to email.
   * 
   * @param string $reply_to
   *   The reply to email.
   */
  public function setReplyTo(string $reply_to);

  /**
   * Gets the Guests Invite Subject
   */
  public function getGuestsInviteSubject(); 

  /**
   * Sets the Guests Invite Subject.
   * 
   * @param string $guests_invite_subject
   *   The guests invite subject.
   */
  public function setGuestsInviteSubject(string $guests_invite_subject);

  /**
   * Gets the Guests Invite Message
   */
  public function getGuestsInviteMessage();

  /**
   * Sets the Guests Invite Message
   * 
   * @param string $guests_invite_message
   *   The guests invite message.
   */
  public function setGuestsInviteMessage(array $guests_invite_message);

  /**
   * Gets the New Accounts Invite Subject
   */
  public function getNewAccountsInviteSubject();

  /**
   * Sets the New Accounts Invite Subject
   * 
   * @param string $new_accounts_invite_subject
   *   The new accounts invite subject.
   */
  public function setNewAccountsInviteSubject(string $new_accounts_invite_subject);

  /**
   * Gets the New Accounts Invite Message
   */
  public function getNewAccountsInviteMessage();

  /**
   * Sets the New Accounts Invite Message
   * 
   * @param string $new_accounts_invite_message
   *   The new accounts invite message.
   */
  public function setNewAccountsInviteMessage(array $new_accounts_invite_message);

  /**
   * Gets the Existing Accounts Invite Subject
   */
  public function getExistingAccountsInviteSubject();

  /**
   * Sets the Existing Accounts Invite Subject
   * 
   * @param string $existing_accounts_invite_subject
   *   The existing accounts invite subject.
   */
  public function setExistingAccountsInviteSubject(string $existing_accounts_invite_subject);

  /**
   * Gets the Existing Accounts Invite Message
   */
  public function getExistingAccountsInviteMessage();

  /**
   * Sets the Existing Accounts Invite Message
   * 
   * @param string $existing_accounts_invite_message
   *   The existing accounts invite message.
   */
  public function setExistingAccountsInviteMessage(array $existing_accounts_invite_message);

  /**
   * Get the node id.
   */
  public function getNode();

  /**
   * Set the node id.
   * 
   * @param string $node
   *   Set the node id.
   */
  public function setNode(string $node);

}
