<?php

namespace Drupal\social_event_invite_flow;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an event invite settings entity type.
 */
interface EventInviteSettingsInterface extends ConfigEntityInterface {

  /**
   * Invite mode for existing accounts with no webform involved.
   */
  const InviteModeExistingAccountsDefaultEnrollment = 'default_enrollment';

  /**
   * Invite mode for existing accounts by webform.
   */
  const InviteModeExistingAccountsWebformEnrollment = 'webform_existing_accounts';

  /**
   * Invite mode for new accounts with no webform involved.
   */  
  const InviteModeNewAccountsGuestEnrollment = 'guest_enrollment'; 

  /**
   * Invite mode for new accounts by webform.
   */
  const InviteModeNewAccountsWebformEnrollment = 'webform_new_accounts';
  

  /**
   * Get invite mode existing accounts
   */
  public function getInviteModeExistingAccounts();

  /**
   * Set invite mode existing accounts
   */
  public function setInviteModeExistingAccounts(string $invite_mode_existing_accounts);

  /**
   * Get invite mode new accounts
   */
  public function getInviteModeNewAccounts();

  /**
   * Set invite mode new accounts
   */
  public function setInviteModeNewAccounts(string $invite_mode_new_accounts);

  /**
   * Get selected webform existing accounts
   */
  public function getSelectedWebformExistingAccounts();

  /**
   * Set selected webform existing accounts
   */
  public function setSelectedWebformExistingAccounts(string $selected_webform_existing_accounts);

  /**
   * Get selected webform new accounts
   */
  public function getSelectedWebformNewAccounts();

  /**
   * Set selected webform new accounts
   */
  public function setSelectedWebformNewAccounts(string $selected_webform_new_accounts);

  /**
   * Get access token
   */
  public function getAccessToken();

  /**
   * Set access token
   */
  public function setAccessToken(string $access_token);

  /**
   * Get enalbe shareable link
   */
  public function getEnableShareableLink();

  /**
   * Set enable shareable link
   */
  public function setEnableShareableLink(bool $enable_shareable_link);  
  
  /**
   * Get node
   */
  public function getNode();

  /**
   * Set node
   */
  public function setNode(string $node);


}
