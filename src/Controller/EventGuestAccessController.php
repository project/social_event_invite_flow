<?php

namespace Drupal\social_event_invite_flow\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\social_event_invite_flow\Service\EventInviteFlowService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\node\NodeInterface;
use Drupal\Core\Render\Markup;
use Drupal\views\Views;
use Drupal\Core\Cache\Cache;

/**
 * Returns responses for Social event invite flow routes.
 */
class EventGuestAccessController extends ControllerBase {

  /**
   * The social_event_invite_flow.invite_flow_service service.
   *
   * @var \Drupal\social_event_invite_flow\Service\EventInviteFlowService
   */
  protected $inviteFlowService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The controller constructor.
   *
   * @param \Drupal\social_event_invite_flow\Service\EventInviteFlowService $invite_flow_service
   *   The social_event_invite_flow.invite_flow_service service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(EventInviteFlowService $invite_flow_service, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match, AccountInterface $account) {
    $this->inviteFlowService = $invite_flow_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('social_event_invite_flow.invite_flow_service'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('current_user')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $event_content = [];

    // Get the access token
    $access_token = \Drupal::request()->query->get('access_token');

    if (isset($access_token) && !empty($access_token)) {
      $event_invite_settings = \Drupal::entityTypeManager()->getStorage('event_invite_settings')->loadByProperties(['access_token' => $access_token]);
      if ($event_invite_setting = reset($event_invite_settings)) {
        $node = $event_invite_setting->getNode();
      }
      else {
        $node = $this->routeMatch->getParameter('node');
      }
    }
    else {
      $node = $this->routeMatch->getParameter('node');
    }

    if (!$node instanceof NodeInterface) {
      $node = $this->entityTypeManager->getStorage('node')->load($node);
    } 


    $start_event = $node->field_event_date->date;
    $end_event = $node->field_event_date_end->date;
    $current_date = time();
    $event_title = $node->getTitle();
 
    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    $event_description = $node->body->view(['label' => 'hidden']);

    $event_content['description'] = $event_description;

    $event_managers = $node->field_event_managers->view();


    $event_start = $node->field_event_date->date;
 
    $event_content['event_start'] = [
      '#theme' => 'event_date_advanced',
      '#node' => $node,
      '#view_mode' => 'hero'
    ];


    // First let's check if we have an account here
    if ($this->account->isAuthenticated()) {
      $form = \Drupal::formBuilder()->getForm('\Drupal\social_event_invite_flow\Form\JoinVirtualEventAsUser', $node->id());
    }
    else {
      $form = \Drupal::formBuilder()->getForm('\Drupal\social_event_invite_flow\Form\JoinVirtualEventAsGuest', $node->id());
    }    

    if ($current_date > $start_event->getTimestamp() && $current_date < $end_event->getTimestamp()) {     
      $event_image = FALSE;
    }
    else {

      if (isset($node->field_event_image->target_id)) {
        $event_image = $node->field_event_image->view([
          'type' => 'image',
          'label' => 'hidden',
          'settings' => [
            'image_style' => 'large',
          ]      
        ]);
      }
      else {
        $event_image = FALSE;
      }
    }  

    $build['content'] = [
      '#theme' => 'social_event_invite_flow_guest_access_content',
      '#event_title' => $event_title,
      '#event_managers' => $event_managers,
      '#content' => $event_content,
      '#form' => $form,
      '#event_image' => $event_image,
      '#attached' => [
        'drupalSettings' => [
          'event_start_timestamp' => $event_start->getTimestamp(),
          'event_end_timestamp' => $end_event->getTimestamp(), 
        ],
        'library' => [
          'social_event_invite_flow/flow_design_page',
        ]
      ],
      '#cache' => [
        'keys' => ['entity_view', 'node', $node->id()],
        'contexts' => ['languages'],
        'tags' => $node->getCacheTags(),
        'max-age' => Cache::PERMANENT, 
      ] 
    ];

    return $build;

  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {

    $allowed = FALSE;

    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.
    $access_token = \Drupal::request()->query->get('access_token');

    // Check access token
    $event_invite_settings = \Drupal::entityTypeManager()->getStorage('event_invite_settings')->loadByProperties(['access_token' => $access_token]);

    // Allow for authenticated users with access to the node
    $node = $this->routeMatch->getParameter('node');
    if (!$node instanceof NodeInterface) {
      $node = $this->entityTypeManager->getStorage('node')->load($node);
    }

   
    // Allow access to authenticated users with access to the node.
    if ($node instanceof NodeInterface && $node->access('view', $account)) {
      $allowed = TRUE;
    }
    elseif ($event_invite_setting = reset($event_invite_settings)) {
      $allowed = TRUE;
      
      $nid = $event_invite_setting->getNode();
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
      $start_event = $node->field_event_date->date;
      $end_event = $node->field_event_date_end->date;
      $current_date = time();
      if ($current_date > $end_event->getTimestamp()) {
        $allowed = FALSE;
      }      
    }   
  
    return AccessResult::allowedIf($allowed);

  }  

}
